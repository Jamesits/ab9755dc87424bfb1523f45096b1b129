#!/usr/bin/env python3
# by James Swineson, 2017-02-05
# https://gist.github.com/Jamesits/ab9755dc87424bfb1523f45096b1b129

import re, collections, codecs

class TKVParser:
    # string.split with a list of delimiters support
    @staticmethod
    def __split(delimiters: list, string: str, maxsplit: int=0) -> list:
        regexPattern = '|'.join(map(re.escape, delimiters))
        return re.split(regexPattern, string, maxsplit)

    # This is used to match key=value pairs
    # can process the situation when value is a TKV string
    @staticmethod
    def __kv_matcher(input_string: str, pair_delimiters: list, kv_separators: list) -> collections.OrderedDict:
        kvdict = collections.OrderedDict()
        while len(input_string) > 0:
            # get key first
            current_key, input_string = TKVParser.__split(kv_separators, input_string, maxsplit=1)
            current_key = current_key.strip()
            # then try if the value is an TKV format string
            tkv_value = TKVParser.__tkv_matcher(input_string)
            # if not, then treat as string
            if tkv_value == None:
                current_value, input_string = TKVParser.__split(pair_delimiters, input_string, maxsplit=1)
                kvdict[current_key] = current_value.strip()
            # if is TKV format, parse recursively
            else:
                input_string = input_string[tkv_value["length"]:]
                kvdict[current_key] = tkv_value

        return kvdict

    # This is used to search for TKV string like
    # Type[Key=Value, ...]
    @staticmethod
    def __tkv_matcher(input_string: str) -> dict:
        #Try if we can get a match right from the string head
        ret = re.match(r"^(\w+)\[(.*)\]", input_string)
        if ret == None:
            return None
        else:
            return {
                "type": ret[1],
                "length": len(ret[0]),
                "data": TKVParser.__kv_matcher(ret[2], [';', ','], ['=']),
            }

    def parse_stream(self, stream: codecs.StreamReader, callback):
        while True:
            # input
            current_line = stream.readline()
            # if we got EOF
            if len(current_line) == 0:
                break
            # process data
            result = self.__tkv_matcher(current_line)
            # if not in TKV format then ignore this line
            if result == None:
                continue
            # output
            callback(result)

class CSVGenerator:
    def __init__(self, csv_filter: collections.OrderedDict, separator: str):
        self.csv_filter = csv_filter
        self.separator = separator

    def header(self) -> str:
        return self.separator.join(self.csv_filter.keys())

    def content(self, line: collections.OrderedDict) -> str:
        values = []
        for item in self.csv_filter.values():
            item_path = item.split('.')
            value = line['data'][item_path.pop(0)]
            while len(item_path) > 0:
                value = value['data'][item_path.pop(0)]
            values.append(value)
        return self.separator.join(values)

if __name__ == "__main__":
    # test case
    parser = TKVParser()
    generator = CSVGenerator(
        # csv header, data path in original struct
        collections.OrderedDict([
            ('time',            'time'),
            ('price',           'price'),
            ('initial_price',   'book.bid0'),
            ('final_price',     'book.ask4'),
        ]),
        # csv separator
        ','
    )
    print(generator.header())
    def print_csv_content(dataline):
        print(generator.content(dataline))

    with open("examples/data.log") as f:
        parser.parse_stream(f, print_csv_content)
